<?php
get_header();
?>
<div class="cookie-block section-two-columns">
	<div class="shell">
		<div class="section__inner tile">

    <?php
    if(have_posts()):
        while(have_posts()): the_post();
            get_template_part('partials/content', 'page');
        endwhile;
    endif;
    ?>



		</div>
	</div>
</div>

<?php get_footer(); 