//var $taxonomy = document.querySelector('#taxonomy-contract');
//var $checkboxes = document.querySelectorAll('#contract-all input[type=checkbox]')

(function($){
    //console.log(cpt_checkboxes_radio.conversion)
    if(cpt_checkboxes_radio.conversion) {
        var items = cpt_checkboxes_radio.conversion;
        for (var i = 0; i < items.length; i++) {
            var checkboxName = items[i];
            $('#' + checkboxName + '-all input[type=checkbox]').each(function () {
                $(this).attr('type', 'radio');
            });
            $('#' + checkboxName + '-tabs, #' + checkboxName + '-adder').remove();
        }
    }
    if(cpt_checkboxes_radio.mandatory){
        var items = cpt_checkboxes_radio.mandatory;
        for (var i = 0; i < items.length; i++) {
            var checkboxName = items[i];
            $('<span />').addClass('acf-required').text(' *').appendTo($('#' + checkboxName + 'div .hndle'));
        }

        $('#post').on('submit', function(e){
            //e.preventDefault();
            //e.stopImmediatePropagation();
            return !checkMandatory();
        })

        function checkMandatory(){
            var hasErrors = false;
            var $firstErrorWrap;
            for (var i = 0; i < items.length; i++) {
                var checkboxName = items[i];
                if($('#'+checkboxName+'checklist input[name="tax_input['+checkboxName+'][]"]:checked').length == 0 ){
                    hasErrors = true;
                    var $taxonomyWrap = $('#taxonomy-' + checkboxName + '');
                    if($taxonomyWrap.find('.acf-error-message').length == 0) {
                        $('<div />').html('<div class="acf-error-message"><p>value is required</p></div>')
                            .addClass('acf-field').css('margin', 0)
                            .prependTo($taxonomyWrap);
                    }
                    if(!$firstErrorWrap) $firstErrorWrap = $taxonomyWrap;
                }else{
                    $('#taxonomy-'+checkboxName+ ' .acf-error-message').remove();
                }
            }

            //snap document to the first error field
            if($firstErrorWrap) {
                var yPos = $firstErrorWrap.offset().top - 100;
                $('html, body').animate({
                    scrollTop: yPos + 'px'
                }, 600);
            }
            return hasErrors;
        }
    }
})(jQuery);
