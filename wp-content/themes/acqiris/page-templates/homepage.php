<?php
/*
Template Name: Homepage
*/
get_header();
    $sliderContents = get_field('slider_contents',$post->ID);
    if(!empty($sliderContents)):
?>
        <script>
            //allow section navigation by js
            window.isPageOnePage = true;
        </script>
    <div class="intro">
        <div class="slider slider--intro swiper-container">
            <div class="slider__slides swiper-wrapper">
                <?php foreach ($sliderContents as $slider) :
                    $sliderImage = wp_get_attachment_image_src( $slider['image'], 'sliderImage');
                    ?>
                        <div class="slider__slide swiper-slide" style="background-image: url(<?php echo $sliderImage[0]; ?>">
                            <div class="shell">
                                <div class="slider__content">
                                    <?php if(!empty($slider['title']))?><h3><?=$slider['title']?></h3>
                                    <?php if(!empty($slider['link']))?><h3> <a href="<?=$slider['link']?>" class="btn">Learn more</a></h3>
                                </div><!-- /.slider__content -->
                            </div><!-- /.shell -->
                        </div><!-- /.slider__slide -->
                 <?php endforeach; ?>

            </div><!-- /.slider__slides -->

            <div class="slider__actions">
                <div class="swiper-button-prev">
                    <i class="ico-arrow-left"></i>
                </div>
                <div class="swiper-button-next">
                    <i class="ico-arrow-right"></i>
                </div>
            </div><!-- /.slider__actions -->
        </div><!-- /.slider slider-/-intro -->
    </div><!-- /.intro -->
<?php endif; ?>

    <div class="main">
        <?php
        while(have_posts()): the_post();
            if( have_rows('flexible_contents') ):
                // loop through the rows of data
                while (have_rows('flexible_contents') ) : the_row();
                    $layout = get_row_layout();
                    $tpl = get_template_directory().'/partials/flexible-contents/'.$layout.'.php';
                    if(file_exists($tpl)) {
                        include $tpl;
                    }
                endwhile;
            endif;
        endwhile;
        ?>
    </div><!-- /.main -->
    <!--#include virtual="partials/footer.shtml" -->
<?php get_footer(); ?>