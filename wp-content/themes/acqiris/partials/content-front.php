    <div class="swiper-container" >
        <div id="main-gallery">
        <?php echo \App\Renderer::$engine->render('banner', [
        'items'=>get_field('banners')
        ]);
        ?>
        <!-- Add Pagination -->
        <!--<div class="swiper-pagination"></div>-->
        <!-- Add Arrows -->
        <div class="swiper-button-next hidden-xs"></div>
        <div class="swiper-button-prev hidden-xs"></div>
        </div>
       
    </div>
