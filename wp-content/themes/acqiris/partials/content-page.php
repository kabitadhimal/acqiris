<?php /*
<?php the_content(); ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
*/ ?>
    <?php if(!is_front_page() && !get_field('hide_title')):?>
    <div class="section-gap">
    <h2 class="title-decorate"><?php the_title(); ?></h2>
    <?php the_content(); ?>
    </div>
    <?php endif; ?>



