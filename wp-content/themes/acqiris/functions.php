<?php
/**
 * Procab Theme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 *       Functions that are not pluggable (not wrapped in function_exists()) are
 *       instead attached to a filter or action hook.
 *
 *       For more information on hooks, actions, and filters,
 *       {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Swissroc
 * @since Swissroc 1.0
 */

/**
 * Custom debugging function.
 * Can be commented in production site.
 */
if (! function_exists('pr')) {

    /**
     * Function to add <pre> tag before and after a variable of print_r
     *
     * @param mixed $var
     *            variable to be debugged
     * @param boolean $die
     *            if true die is called after print_r
     */
    function pr($var = '', $die = false)
    {
        $array = debug_backtrace();
        echo '<br/>Debugging from ' . $array[0]['file'] . ' line: ' . $array[0]['line'];

        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if ($die !== FALSE) {
            die();
        }
    }
}

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Romain Jerome 1.0
 */
if (! function_exists('procab_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @sinceprocab 1.0
     */
    function procab_setup()
    {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based onprocab, use a find and replace
         * to change 'procab' to the name of your theme in all the template files
         */
        load_theme_textdomain(TD);

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(158, 240, true);

        // used in post column
        add_image_size('sliderImage', 1900, 500, true);
        add_image_size('productImage', 960, 480, true);


        // Registers a single custom Navigation Menu in the custom menu editor
        register_nav_menu('primary', __('Primary Menu', 'procab'));
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat'
        ));
    }

endif; //procab_setup
add_action('after_setup_theme', 'procab_setup');

include get_template_directory() .'/src/shortcodes/shortcode-functions.php';
include get_template_directory() .'/src/shortcodes/shortcodes.php';

/*require TEMP_DIR .'/functions/kd-custom-posts.php';
include TEMP_DIR .'/includes/shortcodes.php';
include TEMP_DIR .'/functions/shortcode-functions.php';*/

/**
 * Register widget area.
 *
 * @since procab 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function procab_widgets_init()
{
    register_sidebar(array(
        'name' => __('Widget Area', TD),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', TD),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));
}
add_action('widgets_init', 'procab_widgets_init');

/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */

/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */
function procab_scripts() {

    wp_enqueue_style('swiper-min', get_template_directory_uri().'/vendor/swipermaster/swiper.min.css', array(), '1.0');
    wp_enqueue_style('style', get_template_directory_uri().'/css/style.css', array(), '1.1');

    //WordPress Default JQuery
    wp_enqueue_script('jquery');

    wp_register_script('swipermaster', get_template_directory_uri().'/vendor/swipermaster/swiper.min.js', array(), '1.0', false);
    wp_register_script('popup', get_template_directory_uri().'/vendor/jquery.magnific-popup.min.js', array(), '1.0', true);
    wp_register_script('function', get_template_directory_uri().'/js/functions.js', array(), '1.1', true);

    wp_enqueue_script(array(
        'swipermaster',
        'popup',
        'function'
    ));
}
add_action('wp_enqueue_scripts', 'procab_scripts');



/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Procab 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function procab_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'RJ' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'procab_wp_title', 10, 2 );



remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 12);

/*
 * Remove illegal characters from files during uploading on wordpress
 */
add_filter('wp_handle_upload_prefilter', 'custom_upload_filter', 1, 1);

function custom_upload_filter($file)
{
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $file['name']);
    return $file;
}

add_action('template_redirect', 'procab_template_redirect');
function procab_template_redirect(){
    if (is_author()){
        wp_redirect( home_url() ); exit;
    }
}

/*
 * Removes default extracts and replaces them with new blocks
 */
function procab_replace_post_excerpt()
{
    foreach (array(
                 "post",
                 "page"
                 //   "product"
             ) as $type) {
        remove_meta_box('postexcerpt', $type, 'normal');
        add_meta_box('postexcerpt', __('Excerpt'), 'procab_create_excerpt_box', $type, 'normal');
    }
}
add_action('admin_init', 'procab_replace_post_excerpt');

function procab_create_excerpt_box()
{
    global $post;
    $id = 'excerpt';
    $excerpt = procab_get_excerpt($post->ID);

    wp_editor($excerpt, $id);
}

function procab_get_excerpt($id) {
    global $wpdb;
    $row = $wpdb->get_row("SELECT post_excerpt FROM $wpdb->posts WHERE id = $id");
    return $row->post_excerpt;
}

/* Create the options page that we will later set "global"
 */

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Footer Options',
        'menu_title' => 'Footer Options'
    ));
}


function procab_duplicate_featured_image( $post_id ) {

    if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;

    $capability = ( 'page' == $_POST['post_type'] ) ? 'edit_page' : 'edit_post';

    if( is_user_logged_in() && current_user_can( $capability, $post_id ) ) {

        $is_duplicate = get_post_meta( $post_id, '_icl_lang_duplicate_of', true );

        if( !empty( $is_duplicate ) && is_numeric($is_duplicate) ){
            $thumbnail_id = get_post_meta( $is_duplicate, '_thumbnail_id', true );
            update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
        }
    }

}
add_action( 'save_post', 'procab_duplicate_featured_image' );
/*
 * More Shortcode tag
 */
 
 /*
 * Pagination
 */
/*
 * Pagination
 */
function custom_pagination($numpages = '', $pagerange = '', $paged='', $base = '') {
    if (empty($pagerange)) {
        $pagerange = 2;
    }
    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    $base = ($base) ? : get_pagenum_link(1);

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */

    $pagination_args = array(
        //'base'            => get_pagenum_link(1) . '%_%',
        'base'            => $base.'%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => __('&laquo;'),
        'next_text'       => __('&raquo;'),
        'type'            => 'plain',
        'add_args'        => false,
        'add_fragment'    => '',
    );

    $paginate_links = paginate_links($pagination_args);
  
    $paginate_links = str_replace('<a','<li><a', $paginate_links);
    $paginate_links = str_replace('/a>','/a></li>', $paginate_links);
    $paginate_links = str_replace("<span aria-current='page' class='page-numbers current'",'<li class="active"><span', $paginate_links);
    $paginate_links = str_replace("<span class='page-numbers current'",'<li class="active"><span', $paginate_links);
    $paginate_links = str_replace('<span class="page-numbers dots"','<li class="disabled"><span', $paginate_links);
    $paginate_links = str_replace('</span>','</span></li>', $paginate_links);

    /*
  $paginate_links = str_replace('<a','<li><a', $paginate_links);
    $paginate_links = str_replace('/a>','/a></li>', $paginate_links);
    $paginate_links = str_replace("<span aria-current='page' class='page-numbers current'",'<li class="active"><span', $paginate_links);
    $paginate_links = str_replace('<span class="page-numbers dots"','<li class="disabled"><span', $paginate_links);
    $paginate_links = str_replace('</span>','</span></li>', $paginate_links);
     */

    if ($paginate_links) {
        echo "<nav class='custom-pagination'><ul class='page-numbers'>";
        echo $paginate_links;
        echo "</ul></nav>";
    }

}
