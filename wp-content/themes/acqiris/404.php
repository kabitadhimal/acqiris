<?php get_header(); ?>

<div class="section-gap section-gap--huge-top text-center">
    <h1>Sorry, this page does not exist</h1>
    <div class="title">
        <p>The content you are looking for does not exist or has been removed</p>
        <p><a href="<?= home_url() ?>" rel="noreferrer">Go back to the homepage?</a></p>
    </div>
</div>

<?php get_footer(); ?>