<footer class="footer js-section" id="section-contact">
    <div class="footer__content">
        <div class="shell">
            <div class="cols cols--flex">
                <div class="left-col has-border">
                    <div class="cols cols--flex">
                        <div class="left-col sub-col">
                            <div class="contacts">
                                <div class="contacts__head list-inline">
                                    <?php if(empty(get_field('headquarter_title','options')))?> <h5><?=get_field('headquarter_title','options')?></h5>
                                    <a href="https://www.google.com/maps/place/Acqiris+SA/@46.1689133,6.1017803,17z/data=!3m1!4b1!4m5!3m4!1s0x478c7b719f50c475:0x44df66003e27a36e!8m2!3d46.1689133!4d6.103969" class="btn-map" target="_blank">
                                        <i class="ico-map-pin"></i>
                                    </a>

                                </div>

                                <div class="contacts__head">
                                    <?php  echo !empty(get_field('headquarter_content','options'))?get_field('headquarter_content','options'):'';  ?>
                                </div>
                                <?php
                                 $emailID = !empty(get_field("email_id"))?get_field("email_id"):'';
                                 if(!empty($emailID)):
                                ?>
                                <div class="contact--by-mail">
                                    <a href="mailto:<?=$emailID?>"><?=$emailID?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="right-col sub-col">
                            <div class="contacts">

                                <?php
                                $supportBlock = get_field("support_block","options");
                                if(!empty($supportBlock)):
                                ?>
                                <div class="contacts__head">
                                    <?php if(!empty($supportBlock['title'])): ?> <h5><?=$supportBlock['title']?></h5><?php endif; ?>
                                    <div class="contact--by-mail">
                                        <a href="mailto:<?=$supportBlock['link']?>"><?=$supportBlock['link']?></a>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php
                                $careerBlock = get_field("career_block","options");
                                if(!empty($careerBlock)):
                                ?>
                                <div class="contacts__head career">
                                    <div class="contacts__head">
                                        <?php if(!empty($careerBlock['title'])): ?> <h5><?=$careerBlock['title']?></h5><?php endif; ?>
                                        <?php if(!empty($careerBlock['text'])): ?><p><?=$careerBlock['text']?></p><?php endif; ?>
                                    </div>
                                    <div class="contact--by-mail">
                                        <?php if(!empty($careerBlock['link'])): ?>
                                            <a href="mailto:<?=$careerBlock['link']?>"><?=$careerBlock['link']?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                </div>
                </div>


                <div class="right-col">
                    <div class="cols cols--flex">
                        <?php
                    $contacts = get_field('global_contacts','options');
					 $contactSlice = array_slice($contacts,0,3,true);
					 if(!empty($contactSlice)):
                        ?>
                        <div class="left-col sub-col">
                            <div class="contacts">
                                <?php foreach ($contactSlice as $contact) : ?>
                                <div class="contacts__head">
                                    <?php if(!empty($contact['contact_name'])): ?><h5><?=$contact['contact_name']?></h5> <?php endif; ?>
                                    <div class="contact--by-mail">
                                        <?php if(!empty($contact['email_address'])) :?> <a href="mailto:<?=$contact['email_address']?>"><?=$contact['email_address']?></a><?php endif; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="right-col sub-col">
                            <?php // var_dump($contacts);
                            $remContactSlice = array_slice($contacts,3);
                            if (!empty($remContactSlice)):
                                foreach ($remContactSlice as $contact) : ?>
                                <div class="contacts__head">
                                    <?php if(!empty($contact['contact_name'])): ?><h5><?=$contact['contact_name']?></h5> <?php endif; ?>
                                    <div class="contact--by-mail">
                                        <?php if(!empty($contact['email_address'])) :?> <a href="mailto:<?=$contact['email_address']?>"><?=$contact['email_address']?></a><?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; endif; ?>

                            <div class="contacts__head wechat-block">
                                <?php $weChat = get_field("wechat","options"); ?>
                                 <?php if(!empty($weChat['title'])): ?><p><?=$weChat['title']?></p><?php endif;
                                 $link = $weChat['link'];
                                 ?>
                                <div class="contact--by-wechat">
                                    <p>Contact us via WeChat:</p>
                                    <a href="#qr-scan" class="open-popup-link"><?php echo "See QR Code"; ?></a>
                                    <?php if(!empty($link)) : ?>
                                            <a href="<?=$link['url']?>">
                                                <?=$link['title']?>
                                            </a>
                                    <?php endif; ?>
                                    <a href="#qr-scan" class="open-popup-link">
                                        <img class="wechat-icon" src="<?php echo get_template_directory_uri()?>/css/images/wechat.png" alt="WeChat">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="qr-scan" class="white-popup mfp-hide">
                <?php $popupTitle = $weChat['popup_title']; ?>
                <?php if(!empty($popupTitle)): ?> <h4><?=$popupTitle?></h4> <?php endif; ?>
                <?php $qrImage = $weChat['qr_code_image'];
                    if(!empty($qrImage)):
                ?>
                    <div class="popup__body">
                        <img src="<?=$qrImage?>" alt="QR Image">
                    </div>
                <?php endif; ?>
            </div>
            <a href="#wrapper" class="btn-scrolltop js-scroll-to">
                <i class="ico-arrow-up"></i>
            </a>
        </div>
    </div>

    <?php /*
	<div class="footer__content">
		<div class="shell">
			<div class="cols cols--flex">
				<div class="col col--size2">
					<div class="contacts">
						<header class="contacts__head">
							<?php if(empty(get_field('headquarter_title','options')))?> <h5><?=get_field('headquarter_title','options')?></h5>
							<a href="https://www.google.com/maps/place/Acqiris+SA/@46.1689133,6.1017803,17z/data=!3m1!4b1!4m5!3m4!1s0x478c7b719f50c475:0x44df66003e27a36e!8m2!3d46.1689133!4d6.103969" class="btn-map" target="_blank">
								<i class="ico-map-pin"></i>
							</a>
						</header>   <!-- /.contacts__head -->

						<div class="contacts__body">
							<?php  echo !empty(get_field('headquarter_content','options'))?get_field('headquarter_content','options'):'';  ?>
						</div><!-- /.contacts__body -->
					</div><!-- /.contacts -->
				</div><!-- /.col col-/-size2 -->
				<?php
					$contacts = get_field('global_contacts','options');
					if(!empty($contacts)):
				?>
				<div class="col">
					<div class="contact-wrapper">
						<ul class="list-contacts">
							<?php $i = 1; ?>
							<?php foreach ($contacts as $contact): ?>
							<li data-eq-id="<?= $i; ?>">
								 <?php if(!empty($contact['contact_name']))?><h5><?=$contact['contact_name']?></h5>
								<?php if(!empty($contact['email_address']))?> <a href="mailto:<?=$contact['email_address']?>"><?=$contact['email_address']?></a>
							</li>
							<?php $i++; ?>
						<?php endforeach; ?>

						</ul><!-- /.list-contacts -->
					</div><!-- /.contact-wrapper -->


					<?php
					$careerBlock = get_field("career_block","options");
					if(!empty($careerBlock)):
					?>
					<div class="career-block">
						<?php if(!empty($careerBlock['title'])): ?>
							<h5><?=$careerBlock['title']?></h5>
						<?php endif; ?>
						<?php if(!empty($careerBlock['text'])): ?>
							<p><?=$careerBlock['text']?></p>
						<?php endif; ?>
						<?php if(!empty($careerBlock['link'])): ?>
						<a href="mailto:<?=$careerBlock['link']?>"><?=$careerBlock['link']?></a>
						<?php endif; ?>
					</div>
					<?php endif; ?>

				</div><!-- /.col -->
				<?php endif; ?>
			</div><!-- /.cols cols-/-flex -->


		</div><!-- /.shell -->
	</div><!-- /.footer__content -->

*/?>

	<div class="footer__bar">
		<div class="shell">
			<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> Acqiris. All rights reserved.</p><!-- /.copyright -->

			<p class="credits">
				Design by <a href="#">Procab Studio</a>
			</p><!-- /.credits -->
		</div><!-- /.shell -->
	</div><!-- /.footer__bar -->
</footer><!-- /.footer -->

		<?php $cookiePolicy = get_field('cookie_policy','options');
		if(!empty($cookiePolicy)):
		?>
			<div class="cookie-message navbar navbar-fixed-bottom hidden" id="cookie-info">
					<div class="shell">
						<span class="link"><?=$cookiePolicy?></span>
						<span><a class="cookieinfo-close btn btn-sm btn-primary" href="" id="close-cookie-info">I Accept</a></span>
						<span><a class="cookieinfo-close btn btn-sm btn-secondary decline" href="" id="cookie-decline">I Decline</a></span>
				</div>
			</div>
	<?php endif; ?>

<script>
	;(function() {
		// Get saved data from sessionStorage
		var cookieDecline = document.getElementById('cookie-decline');
		cookieDecline.addEventListener('click', function(e) {
			document.querySelector('.cookie-message').classList.add('hidden');
			//localStorage.setItem('barSeen', false);
			window.localStorage.removeItem("barSeen");
			localStorage.clear();
			e.preventDefault();
		});
		var barSeen = localStorage.getItem('barSeen');
		if (!barSeen) {
			document.querySelector('.cookie-message').classList.remove('hidden');
		}

		var barCloser = document.getElementById('close-cookie-info');
		barCloser.addEventListener('click', function(e) {
			document.querySelector('.cookie-message').parentNode.removeChild(document.querySelector('.cookie-message'));
			localStorage.setItem('barSeen', true);

			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-114437045-1');
			
			e.preventDefault();
		});


	})();
</script>
</div><!-- /.wrapper -->
<?php wp_footer(); ?>
</body>
</html>
