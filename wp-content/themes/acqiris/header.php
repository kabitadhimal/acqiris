<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--#include virtual="head.shtml" -->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/css/images/favicon.ico" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114437045-1"></script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="wrapper" id="wrapper">
    <header class="header">
        <div class="shell">
            <div class="header__inner">
                <a href="<?php echo site_url(); ?>" class="logo">acqiris</a>
                <button class="btn-menu" type="button">
                    <span></span>
                </button><!-- /.btn-menu -->
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'div', 'container_class' => 'nav' ) ); ?>
                </nav><!-- /.nav -->
            </div><!-- /.header__inner -->
        </div><!-- /.shell -->
    </header><!-- /.header -->