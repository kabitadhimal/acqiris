<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 11/9/2017
 * Time: 3:43 PM
 */
?>

<section class="section-blocks js-section" id="<?=get_sub_field('id')?>">

    <?php if(!empty(get_sub_field('title'))): ?>
      <div class="block-row js-animation">        
        <h2 class="block-col"><?=get_sub_field('title')?></h2>
      </div>
    <?php endif;

     $synergyContents = get_sub_field('contents');


 if(!empty($synergyContents));
    ?>


    <div class="block-row js-animation">
        <?php
         $sContents = array_slice($synergyContents,0,3);
        $count = 1;
        foreach ($sContents as $content) :
            $contentType = $content['select_content_type'];
            $image = $content['image'];
            if($contentType=='image') :
                    if($count==2) :
                        $contents = '<div class="block block--height'.$count.'" style="background-image: url('.$image.')"></div>';
                    else :
                        $contents = '<div class="block block--height'.$count.' hidden-xs"></div><div class="block block--height'.$count.'" style="background-image: url('.$image.')"></div>';
                    endif;
                else :
                    $backgroundColor = $content['background_color'];
                    $text = $content['text'];
                $contents = '<div class="block block--height1 block--height-text" style="background-color:'.$backgroundColor.'">
                <div class="block__inner">'.$text.'</div></div>';
            endif;
        ?>
        <div class="block-col block-col--1of<?=$count==1?'6':$count?>">
            <?=$contents?>
        </div>

        <?php
         $count++;
        if($count==4) break;
        endforeach; ?>
    </div><!-- /.block-row -->


    <?php


    $sContents = array_slice($synergyContents,3,4);
    ?>


    <div class="block-row js-animation">

        <?php
        $count = 1;
        foreach ($sContents as $content) :
            $contentType = $content['select_content_type'];
            $image = $content['image'];
            if($contentType=='image') :
                if($count==2) :
                ?>
                <div class="block-col block-col--1of6">
                    <div class="block block--height1" style="background-image: url(<?php echo $image; ?>)"></div>
                </div>

            <?php else :
                ?>
                <div class="block-col block-col--1of3">
                    <div class="block block--height3" style="background-image: url(<?php echo $image; ?>)"></div>
                </div>

            <?php endif; ?>

           <?php else :
                $backgroundColor = $content['background_color'];
                $text = $content['text'];
           ?>
            <div class="block-col block-col--1of6">
                <div class="block block--height1 block--height-text" style="background-color:<?=$backgroundColor?>">
                    <div class="block__inner">
                       <?=$text?>
                    </div>
                </div>
            </div>
        <?php
            endif;
            $count++;
        endforeach;
        ?>

    </div><!-- /.block-row -->


    <div class="block-row js-animation">
        <?php
        $sContents = array_slice($synergyContents,7,4);
        $count = 1;
        foreach ($sContents as $content) :
        $contentType = $content['select_content_type'];
        $image = $content['image'];
        if($contentType=='image') :
             if($count==1) :
             ?>
                 <div class="block-col block-col--1of6">
                     <div class="block block--height1" style="background-image: url(<?=$image?>)"></div>
                 </div>

        <?php elseif($count==2):
                 ?>
                 <div class="block-col block-col--1of3">
                     <div class="block block--height3" style="background-image: url(<?=$image?>)"></div>
                 </div>
        <?php elseif ($count==3):
                 ?>

                 <div class="block-col block-col--1of6">
                     <div class="block block--height1" style="background-image: url(<?=$image?>)"></div>
                 </div>
        <?php elseif ($count==4):  ?>
                         <div class="block-col block-col--1of3">
                             <div class="block block--height3" style="background-image: url(<?=$image?>)"></div>
                         </div>
            <?php endif;

         else :
            $backgroundColor = $content['background_color'];
            $text = $content['text'];
       ?>
        <div class="block-col block-col--1of6">
            <div class="block block--height1 block--height-text" style="background-color:<?=$backgroundColor?>">
                <div class="block__inner">
                    <?=$text?>
                </div>
            </div>
        </div>
        <?php endif;
            $count++;
         endforeach;
        ?>

    </div><!-- /.block-row -->

    <div class="block-row js-animation">

        <div class="block-col block-col--1of3 js-animation">
            <?php
            $sContents = array_slice($synergyContents,11,2);
            $count = 1;
            foreach ($sContents as $content) :
            $contentType = $content['select_content_type'];
            $image = $content['image'];
            $backgroundColor = $content['background_color'];
            $text = $content['text'];

            if($count==1) :
            ?>
            <div class="block block--height3" style="background-image: url(<?php echo $image; ?>)"></div>
            <div class="block-col block-col--1of2">
                <div class="block block--height1 hidden-xs"></div>
            </div>
            <?php else :

                if($contentType=="text") { ?>
                <div class="block-col block-col--1of2">
                    <div class="block block--height1 block--height-text" style="background-color:<?=$backgroundColor?>">
                        <div class="block__inner" style="animation: bgcolor 8s linear infinite 0s;">
                            <?=$text?>
                        </div>
                    </div>
                </div>

            <?php }else {
                ?>
                <div class="block-col block-col--1of2">
                    <div class="block block--height1" style="background-image: url(<?=$image?>)"></div>
                </div>
            <?php }

             endif;
            $count++;
                endforeach;
            ?>
        </div>

        <?php
        $sContents = array_slice($synergyContents,13,3);
        $count = 1;
        foreach ($sContents as $content) :
        $contentType = $content['select_content_type'];
        $backgroundColor = $content['background_color'];
        $text = $content['text'];
        $image = $content['image'];
        if($count==1) :
        ?>
        <div class="block-col block-col--1of3">
            <div class="block block--height1" style="background-image: url(<?=$image?>"></div>
        </div>

        <?php elseif($count==2): ?>
            <div class="block-col block-col--1of6">
                <div class="block block--height1 block--height-text"  style="background-color:<?=$backgroundColor?>">
                    <div class="block__inner">
                      <?=$text?>
                    </div>
                </div>
            </div>

       <?php elseif($count==3): ?>

            <div class="block-col block-col--1of6">
                <div class="block block--height1" style="background-image: url(<?=$image?>)"></div>
            </div>

    <?php endif;
        $count++;
        endforeach; ?>

        <div class="block-col block-col--1of6">
            <div class="block block--height1 block--height-btn">
                <a href="#section-contact" class="btn js-scroll-to">Contact Us</a>
            </div>
        </div>

        <div class="block-col block-col--1of6">
            <div class="block block--height1 hidden-xs"></div>
        </div>
    </div>
</section>