<?php
/*
Template Name: Document List
*/
get_header();

$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
//$args = [ 'post_type' => 'post', 'paged' => $paged,'posts_per_page' => 5, 'post_status'=> 'publish'];
$args = ['post_type'=>'post', 'paged' => $paged, 'posts_per_page' => 5, 'orderby' => 'title', 'order'   => 'ASC'];

$taxQuery = [];
$inputCategories = [];

$categories = get_categories( array(
    'orderby' => 'term_id',
));

$categoryList = [];
foreach ($categories as $cat) :
    array_push($categoryList,$cat->term_id);
endforeach;

$sortCriteria = ['date','title'];
if(isset($_GET['sort-by'])) :
    if (in_array($_GET['sort-by'], $sortCriteria)) {
        $sortBy = $_GET['sort-by'];
    }
endif;

if(isset($_GET['cat']) && $_GET['cat']!='all' ) {
    if(in_array($_GET['cat'],$categoryList)) :
        $catID = $_GET['cat'];
    endif;
}


if (!empty($sortBy) && ($sortBy=='date') && !empty($cat)) :
    $args = [ 'post_type' => 'post', 'cat' => $catID, 'paged' => $paged,'posts_per_page' => 5, 'post_status'=> 'publish','orderby' => 'title', 'order'   => 'ASC'];
elseif (!empty($sortBy) && ($sortBy=='title') && !empty($catID)):
    $args = [ 'post_type' => 'post', 'cat' => $catID, 'paged' => $paged,'posts_per_page' => 5, 'post_status'=> 'publish','order'=>'ASC', 'orderby'=>'post_title'];
elseif (!empty($sortBy) && ($sortBy=='title') && empty($catID)) :
    $args = [ 'post_type' => 'post', 'paged' => $paged,'posts_per_page' => 5, 'post_status'=> 'publish','order'=>'ASC', 'orderby'=>'post_title'];
endif;


$docQuery = new WP_Query( $args );

?>

<div class="main">
    <div class="section-group js-section" id="section-products">
        <section class="section-products">
            <header class="section__head">
                <?php the_title('<h2>','</h2>'); ?>
                <?php the_content(); ?>

            </header><!-- /.section__head -->
            <div class="section__body">
                <div class="tabs">
                    <div class="tabs__body">
                        <div class="tab current">
                            <div class="accordion">
                                <div class="accordion__section expanded">
                                    <div class="accordion__body" style="display: block;">
                                        <div class="table">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <form method="get" id="doc-frm" action="<?=$actionUrl?>">
                                                    <th>
                                                        <div class="sorting">
                                                            <label for="field-sort" class="form__label">Sort by:</label>

                                                            <div class="form__controls">
                                                                <div class="select">
                                                                    <select name="sort-by" id="field-sort" class="js-doc-frm-input">
                                                                        <option value="date" <?=($sortBy=='date')?'selected':''?>>Date</option>
                                                                        <option value="title" <?=($sortBy=='title')?'selected':''?>>Title</option>
                                                                    </select>
                                                                </div><!-- /.select -->
                                                            </div><!-- /.form__controls -->
                                                        </div><!-- /.sorting -->
                                                    </th>

                                                    <th>
                                                        <div class="sorting">
                                                            <label for="field-sort" class="form__label">Sort by:</label>
                                                            <div class="form__controls">
                                                                <div class="select">
                                                                    <select name="cat" id="field-sort" class="js-doc-frm-input">
                                                                        <option value="all">Category</option>
                                                                        <?php
                                                                        $categories = get_terms( 'category', array( 'parent' => 0 ) );
                                                                        foreach($categories as $category):
                                                                            ?>
                                                                            <option value="<?=$category->term_id?>" <?=($category->term_id==$catID) ? 'selected' : ''?>><?=$category->name?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div><!-- /.select -->
                                                            </div><!-- /.form__controls -->
                                                        </div>
                                                    </th>
                                                    </form>
                                                </tr>


                                                <?php
                                                if ($docQuery->have_posts()):
                                                while ($docQuery->have_posts()): $docQuery->the_post();
                                                $docLink = !empty(get_field('upload_doc')) ? get_field('upload_doc') : get_field('document_link');
                                                    //$docLink = !empty(get_field('upload_doc')) ? get_field('upload_doc') : get_field('document_link');
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <h5><a href="<?=$docLink?>" target="_blank"><?php the_title(); ?></a></h5>
                                                            <?php if(!empty(get_field('description'))) echo get_field('description'); ?>
                                                            <?php $todaysDate = get_the_date("d-M-Y", $post->ID); ?>
                                                            <p><?php if(!empty(get_field('document_type'))) ?><em><?=get_field('document_type')?></em><span><?php echo $todaysDate; ?></span></p>
                                                        </td>
                                                        <td>
                                                            <div class="file">
                                                                <p>
                                                                    <a href="<?=$docLink?>" target="_blank"><i class="<?=!empty(get_field('icon'))?get_field('icon'):''?>"></i>
                                                                        <?php if(!empty(get_field('document_text'))) ?><?=get_field('document_text')?>
                                                                    </a>
                                                                </p>
                                                            </div><!-- /.file -->
                                                        </td>
                                                    </tr>
                                                <?php endwhile;

                                                ?>
                                                </tbody></table>

                                            <div class="text-center page-links">
                                                <?php
                                                custom_pagination($docQuery->max_num_pages,"",$paged, $actionUrl);
                                                wp_reset_query();
                                                ?>
                                            </div>

                                            <?php  else :
                                            ?>
                                                <div class="empty-result">
                                                    <p>No results matched. Try other options.</p>
                                                </div>
                                        <?php
                                            endif;
                                            wp_reset_postdata();
                                        ?>
                                        </div><!-- /.table -->
                                    </div><!-- /.accordion__body -->
                                </div>
                            </div><!-- /.accordion -->
                        </div><!-- /.tab -->
                    </div><!-- /.tabs__body -->
                </div><!-- /.tabs -->
            </div><!-- /.section__body -->
        </section><!-- /.section-products -->
    </div><!-- /.section-group -->
</div>
<script>
    jQuery( document ).ready(function() {
        var $docForm = jQuery('#doc-frm');
        jQuery('select').change(function(){
            $docForm.submit();
        });

    });
</script>

<style>
    .custom-pagination {
        text-align: center;
    }
    .custom-pagination ul.page-numbers {
        display: inline-block;
        margin-right: -2px;
        margin-left: -2px;
        list-style: none;
    }

    .custom-pagination ul.page-numbers>li {
        padding-left: 2px;
        padding-right: 2px;
        float: left;
        line-height: 1;
    }

    .custom-pagination ul.page-numbers>li>a, .custom-pagination ul.page-numbers>li span {
        display: block;
        margin-bottom: 5px;
        padding: 0px 15px;
        border: 1px solid #181716;
        color: #181716;
        line-height: 38px;
    }

    .custom-pagination ul.page-numbers>li.active>a, .custom-pagination ul.page-numbers>li.active>span, .custom-pagination ul.page-numbers>li>a:hover, .custom-pagination ul.page-numbers>li>span:hover {
        border-color: #029e8e;
        color: #029e8e;
    }
</style>

<?php get_footer; ?>

