<?php

/**
 * hide ACF dashboard on admin
 */
#require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/cpt.php';

/**
 * https://www.advancedcustomfields.com/blog/acf-pro-5-5-13-update/
 * Faster load times!
 * disabe loading of scf custom tags
 */
add_filter('acf/settings/remove_wp_meta_box', '__return_true');



/**
 * add options page
 */
if( function_exists('acf_add_options_page') ) {

    // add parent
    /*
    acf_add_options_page(array(
        'page_title' 	=> 'Home Page Settings',
        'menu_title' 	=> 'Home Settings',
        'redirect' 		=> false
    ));
    */

    /*
    acf_add_options_page(array(
        'page_title' => 'Footer Options',
        'menu_title' => 'Footer Options',
        'redirect' 		=> false
    ));
    */


    acf_add_options_page(array(
        'page_title' 	=> 'Options',
        'menu_title' 	=> 'Options',
        'redirect' 		=> false
    ));

    acf_add_options_page(array(
        'page_title' 	=> 'Alert Options',
        'menu_title' 	=> 'Alert Options',
        'capability' => 'read',
//        'capability' => arr'edit_posts\','
//        'redirect' 		=> false
    ));


    /*
    // add sub page
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Info Settings',
        'menu_title' 	=> 'Info Settings',
        'parent_slug' 	=> $parent['menu_slug'],
    ));
    */

}



/*
add_action('admin_enqueue_scripts', function($hook){
    global $typenow;
    if($typenow == 'page'){
        if($hook == 'post.php' || $hook == 'post-new.php'){
            wp_enqueue_script('fti-acf-customize',plugin_dir_url( __FILE__ ).'assets/js/acf.js', [], false, true);
            wp_enqueue_style('fti-acf-customize-css',plugin_dir_url( __FILE__ ).'assets/css/acf.css', [], false);
        }
    }
});
*/

add_action('admin_enqueue_scripts', function($hook){
    global $typenow;
    if($typenow == 'page'){
        if($hook == 'post.php' || $hook == 'post-new.php'){
            wp_enqueue_script('fti-acf-customize', plugin_dir_url( __FILE__ ).'assets/js/acf.js', [], false, true);
            wp_enqueue_style('fti-acf-customize-css', plugin_dir_url( __FILE__ ).'assets/css/acf.css', [], false);
        }
    }
});


/*function my_load_field($field) {

    $user = wp_get_current_user();
    if($user->roles[0]=='subscriber'){
         var_dump($field);
            if($field['name']=="alert_text") {
                $field['disabled'] = 1;
                $field['readonly'] = 1;
            }
    }

	return $field;
}

add_filter("acf/load_field", "my_load_field");*/


add_filter('acf/prepare_field/name=alert_text', 'my_acf_prepare_field');
function my_acf_prepare_field( $field )
{
    //var_dump($field);
    $user = wp_get_current_user();
    if($user->roles[0]=='subscriber'){
        // set a text field to non editable
        $field['disabled'] = true;
        $field['readonly'] = 1;
    }

  return $field;
}

/**
 * Remove capabilities from editors.
 *
 * Call the function when your plugin/theme is activated.
 */
function wpcodex_set_capabilities() {

    // Get the role object.
    $contributor = get_role( 'contributor' );
    // A list of capabilities to remove from contributor.
    $caps = array(
        'promote_users',
        'edit_users',
        'delete_posts',
        'edit_posts'
    );

    foreach ( $caps as $cap ) {
        // Remove the capability.
        $contributor->remove_cap( $cap );
    }
}
add_action( 'init', 'wpcodex_set_capabilities' );

/*
 *  Prevent access to profile  for contributor and subscrber
 */
add_action( 'admin_menu', 'stop_access_profile' );
function stop_access_profile() {

    $contributor = get_role( 'contributor' );
    // A list of capabilities to remove from editors.
    $user = wp_get_current_user();
    if($user->roles[0]=='contributor' || $user->roles[0]=='subscriber') :
        remove_menu_page( 'profile.php' );
        remove_submenu_page( 'users.php', 'profile.php' );
        if(IS_PROFILE_PAGE === true) {
            wp_die( 'You are not permitted to change your own profile information. Please contact a member of HR to have your profile information changed.' );
        }
    endif;
}

/*
 * Redirect subscriber and contribut the  alert options page
 */

add_action('wp_login','app_redirect_user_by_role', 10, 3);
function app_redirect_user_by_role($user_login, $user)
{
    global $user;
    echo $role_name      = $user->roles[0];
    // redirection for subscribe
    if ( 'subscriber' === $role_name || ('contributor' == $role_name  )  ) {
        $redirect = home_url().'/wp-admin/admin.php?page=acf-options-alert-options';
        wp_redirect($redirect);
        exit;
    }
    $adminRedirect = home_url().'/wp-admin';
    wp_redirect($adminRedirect);
    exit;
}
