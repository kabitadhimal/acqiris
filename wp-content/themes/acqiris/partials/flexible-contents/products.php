<div class="section-group js-section" id="section-products" xmlns="http://www.w3.org/1999/html">
    <section class="section-products">
        <header class="section__head">
            <?php if(!empty(get_sub_field('title'))): ?>
                <h2><?=get_sub_field('title')?></h2>
            <?php endif; ?>
            <?php if(!empty(get_sub_field('sub_content'))): ?>
                <p><?=get_sub_field('sub_content')?></p>
            <?php endif; ?>
        </header><!-- /.section__head -->
        <div class="section__body">
            <div class="tabs">
                <div class="tabs__head">
                    <nav class="tabs__nav">
                        <ul>
                            <?php
                            $products = get_sub_field('products');
                            $count=1;
                            foreach ($products as $product) :
                                $activeClass = !empty($product['highlight_active'])?'class="current"':'';
                                ?>
                                <li <?=$activeClass?>> <a href="#tab<?=$count?>"><?=$product['product_type']?></a></li>
                                <?php $count++;
                            endforeach; ?>
                        </ul>
                    </nav><!-- /.tabs__nav -->
                </div><!-- /.tabs__head -->

                <div class="tabs__body">
                    <?php
                    $productCount=1;
                    foreach ($products as $product) :
                       $activeClass = !empty($product['highlight_active'])?"current":'';
                        ?>
                        <div class="tab <?=$activeClass?>" id="tab<?=$productCount?>">
                            <div class="tile">
                                <?php if(!empty($product['image'])) :
                                    $pImage = wp_get_attachment_image_src($product['image'],'productImage');
                                    ?>
                                    <figure class="tile__image">
                                        <img src="<?=$pImage[0]?>" alt="<?=$product['product_title']?>">
                                    </figure><!-- /.tile__image -->
                                <?php endif; ?>


                                <div class="tile__content">
                                    <div class="tile__entry">
                                        <?php if(!empty($product['product_title'])) : ?>
                                            <h5><?=$product['product_title']?></h5>
                                        <?php endif; ?>
                                        <?php if(!empty($product['product_description'])) : ?>
                                            <div class="tile__text">
                                                <?php

                                                if (strpos($product['product_description'], '[readmore-button]')) {
                                                    $readmore = explode('[readmore-button]',$product['product_description']);
                                                    echo $readmore[0];
                                                    if(!empty($readmore[1])) : ?>
                                                        <span class="show-more__wrapper">
                                                           <span>...</span>
                                                            <button type="button" class="show-more">Read more</button>
                                                        </span> </p>
                                                    <?php endif;
                                                } else  {
                                                    echo $product['product_description'];
                                                }
                                                ?>

                                            </div><!-- /.tile__text -->
                                            <?php if(!empty($readmore[1])) : ?>
                                                <div class="tile__collapsed">
                                                    <?=$readmore[1]?><button type="button" class="show-less">

                                                        <img src="<?php echo get_template_directory_uri(); ?>/css/images/ico-arrow-up-green.png" />   

                                                    </button>
                                                </div><!-- /.tile__collapsed -->
                                            <?php endif; ?>

                                        <?php endif; ?>
                                        <?php if(!empty($product['product_datasheet'])) { ?><a href="<?=$product['product_datasheet']?>" class="btn" target="_blank">Datasheet</a> <?php } ?>
                                    </div><!-- /.tile__entry -->
                                </div><!-- /.tile__content -->
                            </div><!-- /.tile -->
                            <div class="accordion">
                                <div class="accordion__section">
                                    <?php
                                    $accordion = $product['accordian'];
                                     if(!empty($accordion)):
                                    foreach ($accordion as $accordionContents):
                                        if(!empty($accordionContents['title'])): ?>
                                            <div class="accordion__head">
                                                <h4><?=$accordionContents['title']?></h4>
                                            </div><!-- /.accordion__head -->
                                        <?php endif; ?>

                                        <?php if(!empty($accordionContents['contents'])): ?>
                                        <div class="accordion__body">
                                            <?=$accordionContents['contents']?>
                                        </div><!-- /.accordion__body -->
                                    <?php endif;  endforeach;
                                    endif;
                                    ?>
                                </div><!-- /.accordion__section -->

                                <div class="accordion__section">
                                    <?php if(!empty($product['document_library'])) ?><div class="accordion__head"> <h4><?=$product['document_library']?></h4></div><!-- /.accordion__head -->
                                    <div class="accordion__body">
                                        <div class="table">
                                            <table>
                                                <tr>
                                                    <th>
                                                        <div>
                                                            <a href="<?php echo home_url('/document-list'); ?>" target="_blank" class="btn">View All </a>
                                                        </div><!-- /.form__controls -->
                                                    </th>
                                                </tr>
                                            </table>

                                            <table>
                                                <?php
                                                $docCat = $product['documents'];
                                                // $ar = array('post_type'=>'post' ,'category__in'=>$docCat, 'posts_per_page' => 3 , 'orderby' => 'post_date', 'order' => 'DESC');
                                                //   $arg = array('post_type'=>'post','posts_per_page' => 3 ,'orderby' => 'post_date', 'order' => 'DESC');
                                                $arg = array(
                                                    'post_type'      => 'post',
                                                    'category__in'=> $docCat,
                                                    'post_status'    => 'publish',
                                                    'orderby' => 'title',
                                                    'order'   => 'ASC'
                                                );
                                                //$arg = ['post_type'=>'post' , 'orderby' => 'post_date','order' => 'DESC'];
                                                $docQuery = new WP_Query($arg);
                                                $docCount =1;
                                                while ($docQuery->have_posts()): $docQuery->the_post();
                                                    $docLink = !empty(get_field('upload_doc')) ? get_field('upload_doc') : get_field('document_link');

                                                    if($docCount==3) {
                                                        $style = 'style="border-bottom:none;"';
                                                    } else {
                                                        $style='';
                                                    }
                                                    ?>
                                                    <tr <?=$style?>>
                                                        <td>
                                                            <h5><a href="<?=$docLink?>" target="_blank"><?php the_title(); ?></a></h5>
                                                            <?php $todaysDate = get_the_date("d-M-Y", $post->ID); ?>
                                                            <p><?php if(!empty(get_field('document_type'))) ?><em><?=get_field('document_type')?></em></p>
                                                        </td>
                                                        <td>
                                                            <span><?=$todaysDate?></span>
                                                        </td>
                                                        <td>
                                                            <?php if(!empty($docLink) || !empty(get_field('document_text'))): ?>
                                                            <div class="file">
                                                                <p>
                                                                    <a href="<?=$docLink?>" target="_blank"><i class="<?=!empty(get_field('icon'))?get_field('icon'):''?>"></i>
                                                                        <?php if(!empty(get_field('document_text'))) ?><?=get_field('document_text')?>
                                                                    </a>
                                                                </p>
                                                            </div><!-- /.file -->
                                                        <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $docCount++;
                                                    if($docCount==4) break;
                                                endwhile;
                                                wp_reset_postdata();
                                                ?>
                                            </table>

                                        </div><!-- /.table -->
                                    </div><!-- /.accordion__body -->
                                </div><!-- /.accordion__section -->
                            </div><!-- /.accordion -->
                            <?php echo !empty($product['product_detail']) ? $product['product_detail'] : ''; ?>
                        </div><!-- /.tab -->

                        <?php $productCount++;
                    endforeach; ?>
                </div><!-- /.tabs__body -->
            </div><!-- /.tabs -->
        </div><!-- /.section__body -->
    </section><!-- /.section-products -->

    <section class="section-callout">
        <?php if(!empty(get_sub_field('quote'))) : ?>
            <header class="section__head">
                <div class="shell">
                    <blockquote>
                        <p>
                            <span>"</span>
                            <?=get_sub_field('quote')?>
                            <span>"</span>
                        </p>
                    </blockquote>
                    <?php if(!empty(get_sub_field('signature'))) echo '<p>'.get_sub_field('signature').'</p>'; ?>
                </div><!-- /.shell -->
            </header><!-- /.section__head -->
        <?php endif; ?>

        <?php if(!empty(get_sub_field('welcome_image')) || !empty(get_sub_field('map_image'))) : ?>
            <div class="section__image">
                <div class="shell">

                    <div id="cycler">

                        <?php if(!empty(get_sub_field('map_image'))) : ?>
                            <img class="active" src="<?=get_sub_field('map_image')?>" alt="Map Image">
                        <?php endif;?>

                        <?php if(!empty(get_sub_field('welcome_image'))) : ?>
                            <img src="<?=get_sub_field('welcome_image')?>" alt="Welcome Image">
                        <?php endif; ?>

                    </div>

                </div><!-- /.shell -->
            </div><!-- /.section__image -->
        <?php endif; ?>
    </section><!-- /.section-callout -->
</div><!-- /.section-group -->

<script type="text/javascript">
    ;(function($) {
        function cycleImages(){
          var $active = jQuery('#cycler .active');
          var $next = ($active.next().length > 0) ? $active.next() : jQuery('#cycler img:first');
          $next.css('z-index',2);//move the next image up the pile
          $active.fadeOut(1000,function(){//fade out the top image
            $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
              $next.css('z-index',3).addClass('active');//make the next image the top one
          });
        }

        setInterval(cycleImages, 3000);
    })();

</script>