<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 11/9/2017
 * Time: 3:24 PM
 */

?>
<section class="section-two-columns js-section" id="section-about">
    <div class="shell">
        <div class="section__inner">
            <header class="section__head">
                <h2>
                    <?=get_sub_field('title')?>
                </h2>
                    <?=get_sub_field('short_description')?>
                <a href="#<?=get_sub_field("contact_section_id")?>" class="btn js-scroll-to">Contact Us</a>
            </header><!-- /.section__head -->

            <div class="section__body">
                <?=get_sub_field('description')?>
            </div><!-- /.section__body -->
        </div><!-- /.section__inner -->
    </div><!-- /.shell -->
</section><!-- /.section-two-columns -->
